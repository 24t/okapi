package net.sf.okapi.filters.openxml;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipFile;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.FileLocation;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TestPowerpointDocument {
	private XMLFactoriesForTest factories = new XMLFactoriesForTest();
	private FileLocation root;

	@Before
	public void setUp() {
		root = FileLocation.fromClass(getClass());
	}

	@Test
	public void testSlides() throws Exception {
		PowerpointDocument doc = getPowerpointDocument("/slideLayouts.pptx", new ConditionalParameters());
		doc.initialize();
		List<String> expected = new ArrayList<>();
		expected.add("ppt/slides/slide1.xml");
		expected.add("ppt/slides/slide2.xml");
		assertEquals(expected, doc.findSlides());
	}

	@Test
	public void testSlideLayouts() throws Exception {
		PowerpointDocument doc = getPowerpointDocument("/slideLayouts.pptx", new ConditionalParameters());
		doc.initialize();
		List<String> expected = new ArrayList<>();
		expected.add("ppt/slideLayouts/slideLayout1.xml");
		expected.add("ppt/slideLayouts/slideLayout2.xml");
		assertEquals(expected, doc.findSlideLayouts(doc.findSlides()));
	}

	private PowerpointDocument getPowerpointDocument(String resource, ConditionalParameters params) throws Exception {
		OpenXMLZipFile ooxml = new OpenXMLZipFile(
			new ZipFile(root.in(resource).asFile()),
			factories.getInputFactory(),
			factories.getOutputFactory(),
			factories.getEventFactory(),
			StandardCharsets.UTF_8.name(),
			null,
			null,
			null
		);
		return (PowerpointDocument)ooxml.createDocument(params);
	}
}
