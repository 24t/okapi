package net.sf.okapi.filters.openxml;

import javax.xml.stream.events.StartElement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

interface Namespaces2 {
    Namespace get(final String key);

    class Default implements Namespaces2 {
        private final Map<String, Namespace> namespaces;

        Default(final StartElement startElement) {
            this.namespaces = new HashMap<>();
            final Iterator iterator = startElement.getNamespaces();
            while (iterator.hasNext()) {
                final javax.xml.stream.events.Namespace namespace = (javax.xml.stream.events.Namespace) iterator.next();
                namespaces.put(
                    namespace.getPrefix(),
                    new Namespace.Default(namespace.getPrefix(), namespace.getNamespaceURI())
                );
            }
        }

        @Override
        public Namespace get(final String key) {
            return this.namespaces.get(key);
        }
    }
}
